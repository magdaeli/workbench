import csv
import requests
from tqdm import tqdm


input_file = './inst_homepages.csv'
output_file = './output.csv'


def url_check(url):
    status_code_forced_https = None
    try:
        r = requests.get(url, timeout=5)
        status_code_orig = r.status_code
    except Exception as ex:
        ex_type = type(ex).__name__
        status_code_orig = ex_type
    if "http://" in url:
        try:
            r_https = requests.get(url.replace("http://", "https://"), timeout=5)
            status_code_forced_https = r_https.status_code
        except Exception as ex:
            ex_type = type(ex).__name__
            status_code_forced_https = ex_type
    return status_code_orig, status_code_forced_https


with open(output_file, mode='w') as out_file:
    with open(input_file) as in_file:
        next(in_file)  # schiebt den Iterator für die for-Schleife auf die 2. Zeile
        fieldnames = ['id', 'freikost_id', 'value', 'status_code', 'https']
        writer = csv.DictWriter(out_file, fieldnames=fieldnames, delimiter=';')
        reader = csv.reader(in_file, delimiter=',')
        writer.writeheader()
        for row in tqdm(reader):  # startet in der 2. Zeile
            status_code_orig, status_code_forced_https = url_check(row[2])
            writer.writerow({'id': row[0], 'freikost_id': row[1], 'value': row[2], 'status_code': status_code_orig, 'https': status_code_forced_https})
        print('End of file.')
